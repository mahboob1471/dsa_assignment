/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nagarro.dsa.AllClass;







/**
 *
 * @author Mahboob Hasan
 */
import java.util.Scanner;
import com.dsa.library.CustomLinkedList;
import com.dsa.exceptions.DataNotFoundException;
import com.dsa.library.LinkedListNode;

public class LinkedListUse {
    
    static Scanner sc=new Scanner(System.in);
    public static void main(String[] args)  {
       
        CustomLinkedList run=new CustomLinkedList();
        
        int choice=0;
                String ch;
                int value;
                int pos;
                LinkedListNode<Integer> head=null;
                do {
                        System.out.println("--------------------------");
                        System.out.println("Press 1 for Insert");
                        System.out.println("Press 2 for Insert at any position");
                        System.out.println("Press 3 for delete a record");
                        System.out.println("Press 4 for delete a record at any postion");
                        System.out.println("Press 5 for center element of LinkedList");
                        System.out.println("Press 6 for Sort LinkedList");
                        System.out.println("Press 7 for Reverse LinkedList");
                        System.out.println("Press 8 for size of the Linked List");
                        System.out.println("Press 9 for Iterator");
                        System.out.println("Press 10 for Traverse/Print");
                        System.out.println("--------------------------");
                        choice=sc.nextInt();
                        switch(choice) {
                                case 1 : // insert
                                        System.out.println(". . . . . . . . ");
                                        System.out.println("Enter Elements");
                                        int element = sc.nextInt();
                                        head=run.insert(head,element);
                                        System.out.println("Value Inserted");
                                        break;
                                case 2 : // insert at position
                                        System.out.println("=============");
                                        System.out.println("Enter value and position");
                                        value=sc.nextInt();
                                        pos=sc.nextInt();
                                        head=run.insert(head, pos, value);
                                        System.out.println("Value Inserted at particular position");
                                        break;
                                case 3 : // delete last value
                                        System.out.println(". . . . . . . . ");
                                        head=run.delete(head);
                                        System.out.println("last record Deleted");
                                        break;
                                case 4 : // delete at any position
                                        System.out.println(". . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .");
                                        System.out.println("Enter the position which you want to delete");
                                        pos=sc.nextInt();
                                        head=run.delete(head, pos);
                                        break;
                                case 5 : // center
                                        System.out.println("=============");
                                        System.out.println(run.center(head));
                                        System.out.println("=============");
                                        break;
                                case 6 : //sort
                                        head=run.sortLL(head);
                                        System.out.println("-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*");
                                        System.out.println("Linked List have been sorted");
                                        break;
                                case 7 : // reverse
                                        System.out.println("Linked List have been reversed");
                                        System.out.println(". . . . . . ");
                                        head=run.reverse(head);
                                        System.out.println(". . . . . . ");
                                        break;
                                case 8 : // size
                                        System.out.println("=============");
                                        System.out.println("Size of the Linked List is "+run.size(head));
                                        System.out.println("=============");
                                        break;

                                case 9 : //print
                                        System.out.println("=============");
                                        run.print(head);
                                        System.out.println("=============");
                                        break;
                                case 10 : // Traverse/print
                                        System.out.println("=============");
                                        run.print(head);
                                        System.out.println("=============");
                                        break;    

                                default :
                                        System.out.println("*************************");
                                        System.out.println("Please enter valid option");
                                        break;


                }       System.out.println("<><><><><><><><><><><><><><><><><><><><><><><><><>");
                        System.out.println("Do you want to enter more details  (y/n)");
                        System.out.println("___________________________________________________");
                        ch=sc.next();
                        }while(ch.equalsIgnoreCase("Y"));

        
    }
}
