/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nagarro.dsa.AllClass;


import com.dsa.library.CustomQueue;
import com.dsa.exceptions.DataNotFoundException;
import java.util.Scanner;

/**
 *
 * @author Mahboob Hasan
 */

public class QueueUse {
    
    static Scanner sc=new Scanner(System.in);
    public static void main(String[] args)  {
        
        try{
            
        CustomQueue<Integer> queue=new CustomQueue<Integer>(11);
        int choice=0;
        String ch;
        int value;
        int pos;   
    do {
            System.out.println("--------------------------");
            System.out.println("Press 1 for enqueue");
            System.out.println("Press 2 for dequeue");
            System.out.println("Press 3 for front");
            System.out.println("Press 4 for contains");
            System.out.println("Press 5 for size");
            System.out.println("Press 6 for to find center element");
            System.out.println("Press 7 for sort");
            System.out.println("Press 8 for reverse");
            System.out.println("Press 9 for Iterator");
            System.out.println("Press 10 for Traverse/Print");
            System.out.println("--------------------------");
            choice=sc.nextInt();
            switch(choice) {
                    case 1 : // push
                            System.out.println(". . . . . . . . ");
                            System.out.println("Enter Elements");
                            int element = sc.nextInt();
                            queue.enqueue(element);
                            System.out.println("Value enqueued into Queue");
                            break;
                    case 2 : // pop
                            System.out.println("=============");
                            System.out.println(queue.dequeue());
                            System.out.println("Value dequeued");
                            break;
                    case 3 : // peek
                            System.out.println(". . . . . . . . ");
                            System.out.println("front  value");
                            System.out.println(queue.front());
                            break;
                    case 4 : //contains
                            System.out.println(". . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .");
                            System.out.println("Enter any element to check whether it contains or not");
                            value=sc.nextInt();
                            System.out.println(queue.contains(queue,value));

                            break;
                    case 5 : // size
                            System.out.println("=============");
                            System.out.println("Size of the stack is "+queue.size());
                            System.out.println("=============");
                            break;
                    case 6 : //center

                            System.out.println("-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*");
                            System.out.println("center element is "+queue.center(queue));

                            break;
                    case 7 : // sort

                            System.out.println(". . . . . . ");
                            queue.sort(queue);
                            System.out.println("queue have been sorted");
                            break;


                    case 8 : //reverse
                            System.out.println("=============");
                            queue.reverse(queue);
                            System.out.println("queue have been reversed");
                            break;
                    case 9 : // Iterator
                            System.out.println("=============");
                            queue.display(queue);
                            System.out.println("=============");
                            break; 
                    case 10 : // Traverse/print
                            System.out.println("=============");
                            queue.display(queue);
                            System.out.println("=============");
                            break; 

                    default :
                            throw new  DataNotFoundException("Please enter valid option");
                           


    }       System.out.println("<><><><><><><><><><><><><><><><><><><><><><><><><>");
            System.out.println("Do you want to enter more details  (y/n)");
            System.out.println("___________________________________________________");
            ch=sc.next();
            }while(ch.equalsIgnoreCase("Y"));
                }catch(DataNotFoundException e){
                    System.out.println(e);
                }

        
    }
}
