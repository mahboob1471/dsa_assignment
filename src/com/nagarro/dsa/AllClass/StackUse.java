/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nagarro.dsa.AllClass;

/**
 *
 * @author Mahboob Hasan
 */


import com.dsa.library.CustomStack;
import com.dsa.exceptions.DataNotFoundException;
import java.util.Scanner;

public class StackUse {
  public static void main(String[] args) {
    
    Scanner sc=new Scanner(System.in);
    // creating custom stack class object and assigning the default size as 11.
    CustomStack<Integer> stack=new CustomStack<>();
        
                int choice=0;
                String ch;
                int value;
                int pos;
               try{
                do {
                        System.out.println("--------------------------");
                        System.out.println("Press 1 for push");
                        System.out.println("Press 2 for pop");
                        System.out.println("Press 3 for peek");
                        System.out.println("Press 4 for contains");
                        System.out.println("Press 5 for size");
                        System.out.println("Press 6 for to find center element");
                        System.out.println("Press 7 for sort");
                        System.out.println("Press 8 for reverse");
                        System.out.println("Press 9 for Iterator");
                        System.out.println("Press 10 for Traverse/Print");
                        System.out.println("--------------------------");
                        choice=sc.nextInt();
                        switch(choice) {
                                case 1 : // push
                                        System.out.println(". . . . . . . . ");
                                        System.out.println("Enter Elements");
                                        int element = sc.nextInt();
                                        stack.push(element);
                                        System.out.println("Value pushed into stack");
                                        break;
                                case 2 : // pop
                                        System.out.println("=============");
                                        System.out.println(stack.pop());
                                        System.out.println("Value poped");
                                        break;
                                case 3 : // peek
                                        System.out.println(". . . . . . . . ");
                                        System.out.println("top value");
                                        System.out.println(stack.peek());
                                        break;
                                case 4 : //contains
                                        System.out.println(". . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .");
                                        System.out.println("Enter any element to check whether it contains or not");
                                        value=sc.nextInt();
                                        System.out.println(stack.contains(stack,value));
                                        
                                        break;
                                case 5 : // size
                                        System.out.println("=============");
                                        System.out.println("Size of the stack is "+stack.size());
                                        System.out.println("=============");
                                        break;
                                case 6 : //center
                                        
                                        System.out.println("-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*");
                                        System.out.println("center element is "+stack.center(stack));
                                        
                                        break;
                                case 7 : // sort
                                        
                                        System.out.println(". . . . . . ");
                                        stack.sort(stack);
                                        System.out.println("Stack have been sorted");
                                        break;
                                

                                case 8 : //reverse
                                        System.out.println("=============");
                                        stack.reverse(stack);
                                        System.out.println("stack have been reversed");
                                        break;
                                case 9 : // Iterator
                                        System.out.println("=============");
                                        stack.print(stack);
                                        System.out.println("=============");
                                        break; 
                                case 10 : // Traverse/print
                                        System.out.println("=============");
                                        stack.print(stack);
                                        System.out.println("=============");
                                        break; 

                                default :
                                        System.out.println("*************************");
                                        throw new DataNotFoundException("Invalid Data");
                                        //System.out.println("Please enter valid option");
                                       // break;


                }       System.out.println("<><><><><><><><><><><><><><><><><><><><><><><><><>");
                        System.out.println("Do you want to enter more details  (y/n)");
                        System.out.println("___________________________________________________");
                        ch=sc.next();
                        }while(ch.equalsIgnoreCase("Y"));
               }catch(DataNotFoundException e){
                   System.out.println(e);
               }
    }
}
                                
                                