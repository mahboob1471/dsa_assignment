/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nagarro.dsa.AllClass;

/**
 *
 * @author Mahboob Hasan
 */

import com.dsa.library.CustomHashMap;
import com.dsa.exceptions.DataNotFoundException;
import java.util.Scanner;

public class HashMapUse {
    
    public static void main(String[] args){
        CustomHashMap<Integer,Integer> map=new CustomHashMap<>();
        Scanner sc=new Scanner(System.in);
        int choice=0;
                String ch;
                int key;
                int value;
                try{
                do {
                        System.out.println("--------------------------");
                        System.out.println("Press 1 for Insert");
                        System.out.println("Press 2 for Delete");
                        System.out.println("Press 3 for Contains");
                        System.out.println("Press 4 for getValue By Key");
                        System.out.println("Press 5 for Size");
                        System.out.println("Press 6 for Iterator");
                        System.out.println("Press 7 for Traverse/Print");
                        System.out.println("--------------------------");
                        choice=sc.nextInt();
                        switch(choice) {
                                case 1 : // insert
                                        System.out.println(". . . . . . . . ");
                                        System.out.println("Enter Key and value");
                                        key = sc.nextInt();
                                        value=sc.nextInt();
                                        map.insert(key,value);
                                        System.out.println("Record Inserted");
                                        break;
                                case 2 : // removeKey
                                        System.out.println("=============");
                                        System.out.println("Enter key which you want to delete");
                                        key=sc.nextInt();
                                        map.removeKey(key);
                                        System.out.println("Key Deleted");
                                        break;
                                case 3 : // contains
                                        System.out.println(". . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .");
                                        System.out.println("Enter any key to check whether the map contains or not");
                                        key=sc.nextInt();
                                        System.out.println("=============");
                                        System.out.println(map.getValue(key)!=null);
                                        break;
                                case 4 :// getValue By Key
                                        System.out.println("=============");
                                        System.out.println("Enter Key for getting its value");
                                        key=sc.nextInt();
                                        System.out.println("Value is :");
                                        System.out.println(map.getValue(key));
                                        break;
                                case 5 : // size
                                        System.out.println("=============");
                                        System.out.println("Size of the Map is :");
                                        System.out.println(map.size());
                                        System.out.println("=============");
                                        break;
                               
                                case 6 : // Iterator
                                        System.out.println("=============");
                                        map.display();
                                        System.out.println("=============");
                                        break;

                                case 7 : // Traverse/print
                                        System.out.println("=============");
                                        //map.iterator();
                                        map.display();
                                        System.out.println("=============");
                                        break;
                                 
                                default :
                                        
                                        throw new DataNotFoundException("Please enter valid option");
                                       


                }       System.out.println("<><><><><><><><><><><><><><><><><><><><><><><><><>");
                        System.out.println("Do you want to enter more details  (y/n)");
                        System.out.println("___________________________________________________");
                        ch=sc.next();
                        }while(ch.equalsIgnoreCase("Y"));
                }catch(DataNotFoundException e){
                    System.out.println(e);
                }

    }
}
