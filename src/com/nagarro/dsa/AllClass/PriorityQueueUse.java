package com.nagarro.dsa.AllClass;





import com.dsa.library.CustomPriorityQueue;
import com.dsa.exceptions.DataNotFoundException;
import java.util.Scanner;
public class PriorityQueueUse {


        public static void main(String[] args) {
               
            Scanner sc=new Scanner(System.in);
                CustomPriorityQueue pq = new CustomPriorityQueue();
                int choice=0;
                String ch;
                
                do {
                        System.out.println("--------------------------");
                        System.out.println("Press 1 for Enqueue");
                        System.out.println("Press 2 for Dequeue");
                        System.out.println("Press 3 for Peek");
                        System.out.println("Press 4 for Contains");
                        System.out.println("Press 5 for Size");
                        System.out.println("Press 6 for Reverse");
                        System.out.println("Press 7 for Center");
                        System.out.println("Press 8 for Iterator");
                        System.out.println("Press 9 for Traverse/Print");
                        System.out.println("--------------------------");
                        choice=sc.nextInt();
                        switch(choice) {
                                case 1 : // insert
                                        System.out.println(". . . . . . . . ");
                                        System.out.println("Enter Elements");
                                        int element = sc.nextInt();
                                        pq.enqueue(element);
                                        break;
                                case 2 : // removeMax
                                        System.out.println("=============");
                                        System.out.println(pq.dequeue());
                                        break;
                                case 3 : // getMax
                                        System.out.println("=============");
                                        System.out.println(pq.peek());
                                        break;
                                case 4 : // contains
                                        System.out.println(". . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .");
                                        System.out.println("Enter any element to check whether the heap contains or not");
                                        int num=sc.nextInt();
                                        System.out.println("=============");
                                        System.out.println(pq.contains(num));
                                        break;
                                case 5 : // size
                                        System.out.println("=============");
                                        System.out.println(pq.getSize());
                                        System.out.println("=============");
                                        break;
                                case 6 : //Reverse
                                        pq.reverse();
                                        System.out.println("-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*");
                                        System.out.println("Priority Queue have been Reverse");
                                        break;
                                case 7 : // Center
                                        System.out.println(". . . . . . ");
                                        System.out.println(pq.center());
                                        System.out.println(". . . . . . ");
                                        break;
                                case 8 : // Iterator
                                        System.out.println("=============");
                                        pq.iterator();
                                        System.out.println("=============");
                                        break;

                                case 9 : // Traverse/print
                                        System.out.println("=============");
                                        pq.iterator();
                                        System.out.println("=============");
                                        break;

                                default :
                                        System.out.println("*************************");
                                        System.out.println("Please enter valid option");
                                        break;


                }       System.out.println("<><><><><><><><><><><><><><><><><><><><><><><><><>");
                        System.out.println("Do you want to enter more details  (y/n)");
                        System.out.println("___________________________________________________");
                        ch=sc.next();
                        }while(ch.equalsIgnoreCase("Y"));

        }

 }
