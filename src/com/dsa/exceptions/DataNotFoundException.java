package com.dsa.exceptions;

public class DataNotFoundException extends Exception {
	public DataNotFoundException(String str) {
		super(str);
	}
}
