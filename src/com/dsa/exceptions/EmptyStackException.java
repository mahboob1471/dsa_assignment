/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dsa.exceptions;



/**
 *
 * @author Mahboob Hasan
 */
public class EmptyStackException extends Exception{
    public EmptyStackException(String str){
        super(str);
    }
}
