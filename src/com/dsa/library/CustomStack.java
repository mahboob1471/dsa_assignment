/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dsa.library;

/**
 *
 * @author Mahboob Hasan
 */

import com.dsa.exceptions.EmptyStackException;
public class CustomStack<T> {
    private int data[];
    private int topIndex;
    public CustomStack() {
            data=new int[3];
            topIndex=-1;//index of topmost element 	
    }
    public CustomStack(int size) {
            data=new int[size];
            topIndex=-1;//index of topmost element 


}
    public int size() {
            return topIndex+1;
    }
    public void push(int data1) {

            if(topIndex>data.length) {
                    System.out.println("doubleCapacity\n");
                    doubleCapacity();
            }
                    topIndex++;
                    data[topIndex]=data1;	
    }
    private void doubleCapacity() {
            // TODO Auto-generated method stub
            System.out.println("doubleCapacity\n");
            int[] temp=data;
            data= new int[2 * temp.length];
            for(int i=0;i<temp.length;i++) {
                    data[i]=temp[i];
            }

    }
    public int top() throws Exception {
            if(topIndex==-1)
                    throw new EmptyStackException("Stack is Empty");
            return data[topIndex];
    }

    public int pop()   {
            int temp=0;
            try {
            if(topIndex==-1)
                    throw new EmptyStackException("Stack is Empty");
            else {
             temp=data[topIndex];
             topIndex--;
            }
            }catch(EmptyStackException e) {
                    e.printStackTrace();
            }
            return temp;
    }
    public int peek() {
            return data[topIndex];
    }
    public boolean isEmpty() {
            return topIndex==-1;
    }
    public boolean contains(CustomStack<Integer> stack,int value){
       int temp[]=new int[topIndex+1];
       //coping all the stack value into a temp array
       for(int i=0;i<temp.length;i++){
           temp[i]=data[topIndex--];
           
       }
       boolean ans=false;
       for(int i=0;i<temp.length;i++){
           if(temp[i]==value) {
               ans=true;
               break;
           }
        }
       int topIndex=0;
       //reinitializing all the values of stack
       for(int i=temp.length-1;i>=0;i--){
           data[topIndex++]=temp[i];
       }
       
       return ans;
    }
    
    
    //reverse the stack
    public void reverse(CustomStack<Integer> stack){
         CustomStack<Integer> st1=new CustomStack<>();
         CustomStack<Integer> st2=new CustomStack<>();
        
        while(!stack.isEmpty()){
            st1.push(stack.pop());
        }
        while(!st1.isEmpty()){
            st2.push(st1.pop());
        }
        while(!st2.isEmpty()){
            stack.push(st2.pop());
        }
    }
    public void sort(CustomStack<Integer> stack){ 
        CustomStack<Integer> t = new CustomStack<>();
        while(!stack.isEmpty()) {
            int item = stack.pop();
            while(!t.isEmpty() && t.peek() > item) stack.push(t.pop());
            t.push(item);   
        }   
        while(!t.isEmpty()) stack.push(t.pop());
    }
    public int center(CustomStack<Integer> stack){
        try{
            if(topIndex==-1)
                    throw new EmptyStackException("Stack is Empty");
            int n=topIndex+1;
            int i=0;
            while(i!=n/2){
                stack.pop();
                i++;
            }
            return stack.pop();
            
        }catch(EmptyStackException e){
            e.printStackTrace();
        }
         
        return -1;
    }
   
    //printing elements
    public void print(CustomStack<Integer> stack){
        
        while(!stack.isEmpty()){
            System.out.println(stack.pop());
        }
    }
    

  }