/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dsa.library;

/**
 *
 * @author Mahboob Hasan
 */



import java.util.Scanner;


public class CustomLinkedList {
    

    
   public LinkedListNode<Integer> head=null;
   public LinkedListNode<Integer> insert(LinkedListNode<Integer> head,int data){
	
        LinkedListNode<Integer> insert=new LinkedListNode<>(data);
        if(head==null) return insert;
        LinkedListNode<Integer> temp=head;
        while(temp.next!=null){
            temp=temp.next;
        }
        temp.next=insert;
        return head;
	}
   public  LinkedListNode<Integer> insert(LinkedListNode<Integer> head, int pos, int data){
	
        LinkedListNode<Integer> insert=new LinkedListNode<>(data);
        LinkedListNode<Integer> prev=head;
        if(pos==0){
            insert.next=head;
            return insert;
        }
        int count=0;
        while(count<pos-1&& prev!=null){
            count++;
            prev=prev.next;
        }
        if(prev!=null){
            insert.next=prev.next;
            prev.next=insert;
        }
        return head;
	}
    
    public  void print(LinkedListNode<Integer> head) {
        while(head != null) {
            System.out.print(head.data + " ");
            head = head.next;
        }
        
        System.out.println();
    }
    public int size(LinkedListNode<Integer> head){
        int size=0;
        while(head!=null){
            size++;
            head=head.next;
        }
        return size;
    }
    public LinkedListNode<Integer> delete(LinkedListNode<Integer> head) {
		
        if(head==null) return head;
        LinkedListNode<Integer> prev=head;
        int lastIndex=size(head)-1;
        int count=0;
        while(count<lastIndex-1&&prev.next!=null){
            count++;
            prev=prev.next;
        }
        if(prev.next!=null){
            prev.next=prev.next.next;
        }
        
        return head;
	}
     public  LinkedListNode<Integer> delete(LinkedListNode<Integer> head, int pos) {
		
        if(head==null) return head;
        LinkedListNode<Integer> prev=head;
        if(pos==0){
            return head.next;
        }
        int count=0;
        while(count<pos-1&&prev.next!=null){
            count++;
            prev=prev.next;
        }
        if(prev.next!=null){
            prev.next=prev.next.next;
        }
        return head;
	}
    public LinkedListNode<Integer> sortLL(LinkedListNode<Integer> head) {
	
        if(head==null||head.next==null) return head;
       	LinkedListNode<Integer> temp1=head;
        LinkedListNode<Integer> temp2=temp1;
        LinkedListNode<Integer> slow=head;
        LinkedListNode<Integer> fast=head;
        while(fast!=null&&fast.next!=null){
            temp2=slow;
            slow=slow.next;
            fast=fast.next.next;
        }
        temp2.next=null;
        LinkedListNode<Integer> l1=sortLL(temp1);
        LinkedListNode<Integer> l2=sortLL(slow);
        return merge(l1,l2);
	}
    public  LinkedListNode<Integer> merge(LinkedListNode<Integer> head1,LinkedListNode<Integer> head2){
         if(head1==null) return head2;
        if(head2==null) return head1;
        LinkedListNode<Integer> dummy=new  LinkedListNode<Integer>(0);
        LinkedListNode<Integer> temp=dummy;
        while(head1!=null&&head2!=null){
            if(head1.data<head2.data){
                temp.next=head1;
                head1=head1.next;
            }else {
                temp.next=head2;
                head2=head2.next;
            }
                temp=temp.next;
        }
        temp.next=(head1==null)? head2: head1;
        
        return dummy.next;
    }
    public LinkedListNode<Integer> reverse(LinkedListNode<Integer> head){
        if(head==null||head.next==null) return head;
        LinkedListNode<Integer> temp=reverse(head.next);
        head.next.next=head;
        head.next=null;
        return temp;
    }
    public int center(LinkedListNode<Integer> head){
        if(head==null) return 0;
        if(head.next==null) return head.data;
        LinkedListNode<Integer> slow=head;
        LinkedListNode<Integer> fast=head;
        while(fast.next!=null&&fast.next.next!=null){
            slow=slow.next;
            fast=fast.next.next;
        }
        return slow.data;
    }
   
   
    
     
}