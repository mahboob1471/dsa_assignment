package com.dsa.library;


import java.util.*;
public class CustomPriorityQueue {
        public ArrayList<Integer> heap;
        
	public CustomPriorityQueue() {
		heap = new ArrayList<>();
	}
	
	public boolean isEmpty() {
		
	return heap.size()==0;
	}
	public int getSize() {
		
        return heap.size();
	}
	public int peek() {
		
        if(isEmpty()) {
			return Integer.MIN_VALUE;
		}
        return heap.get(0);
	}
        public int indexOf(Object o) {
        if (o != null) {
            for (int i = 0; i < heap.size(); i++)
                if (o.equals(heap.get(i)))
                    return i;
        }
        return -1;
    }
        public boolean contains(int o) {
        return indexOf(o) != -1;
    }
        public int center(){
            int indexOfMiddleNumber = heap.size() / 2;
            for (int index = 0; index < indexOfMiddleNumber; ++index) {
                dequeue();
                }
                int middleNumber = dequeue();
                return middleNumber;
        }
        public void iterator(){
            Iterator value = heap.iterator();
  
       
        System.out.println("The iterator values are: ");
        while (value.hasNext()) {
            System.out.println(value.next());
            }
        }
        public void MaxHeapify(int i)
    {
        int n=heap.size();
        int l = 2*i + 1;
        int r = 2*i + 2;
        int largest = i;
       
        if (l < n&& heap.get(l) > heap.get(i))
            largest = l;
        if (r < n && heap.get(r) > heap.get(largest))
            largest = r;
        if (largest != i)
        {
            // swap arr[i] and arr[largest]
//            int temp = heap.get(i);
//            heap.get(i) = heap.get(largest);
//            arr[largest] = temp;
            Collections.swap(heap, i, largest);
            MaxHeapify(largest);
        }
    }
  
    // This function basically builds max heap
     public void reverse()
    {
        // Start from bottommost and rightmost
        // internal mode and heapify all internal
        // modes in bottom up way
         int n=heap.size();
        for (int i = (n-2)/2; i >= 0; --i)
            MaxHeapify(i);
    }
	public void enqueue(int data) {
		heap.add(data);
		upHeapify();
	}
	public int dequeue() {
		if(isEmpty()) {
			return Integer.MIN_VALUE;
		}
		int temp = heap.get(0);
		heap.set(0, heap.get(heap.size()-1));
		heap.remove(heap.size()-1);
		downHeapify();
		return temp;
	}
	public void upHeapify() {
		int childIndex = heap.size()-1;
		while(childIndex>0) {
			int parentIndex = (childIndex - 1) /2;
			if(heap.get(parentIndex) < heap.get(childIndex) ) {
				int temp = heap.get(childIndex);
				heap.set(childIndex, heap.get(parentIndex));
				heap.set(parentIndex,temp);
				childIndex = parentIndex;
			}
			else {
				break;
			}
		}
	}
	public void downHeapify() {
		int parentIndex = 0;
		int leftChildIndex = 1;
		int rightChildIndex = 2;
		int maxIndex = 0;
		while(leftChildIndex<heap.size()) {
			if( heap.get(leftChildIndex) >  heap.get(maxIndex) ) {
				maxIndex = leftChildIndex;
			}
			if( rightChildIndex<heap.size() && heap.get(rightChildIndex) > heap.get(maxIndex) ) {
				maxIndex = rightChildIndex;
			}
			if(maxIndex!=parentIndex) {
				int temp = heap.get(maxIndex);
				heap.set(maxIndex, heap.get(parentIndex));
				heap.set(parentIndex, temp);
				parentIndex = maxIndex;
				leftChildIndex = 2*parentIndex + 1;
				rightChildIndex = 2* parentIndex + 2;	
			}
			else {
				break;
			}	
		}
	}
}