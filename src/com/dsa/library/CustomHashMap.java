/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dsa.library;

/**
 *
 * @author Mahboob Hasan
 */

import java.util.ArrayList;

public class CustomHashMap <K,V> {

    public static  class MapNode<K,V> {
    K key;
    V value;
    MapNode<K,V> next;
    MapNode(K key,V value){
    this.key=key;
    this.value=value;
    this.next=null;
    }
}
	
	
	ArrayList<MapNode<K,V>> buckets;
	int count;
	int numBuckets;
	
	public CustomHashMap() {
		
		this.buckets = new ArrayList<>();
		this.count = 0;
		this.numBuckets = 5;
		
		for(int i=0;i<numBuckets;i++) {
			
			buckets.add(null);		
		}	
	}
	
	private int getBucketIndex(K key) {
		
		int hc = key.hashCode();
		int index = hc%numBuckets;
		return index;	
	}
	
	public double LoadFactor() {
		
		return (1.0*count)/numBuckets;
	}
	
	private void reHash() {
		
		ArrayList<MapNode<K,V>> temp = buckets;
		buckets = new ArrayList<>();
		
		for(int i=0;i<2*numBuckets;i++) {
			buckets.add(null);
		}
		
		count = 0;
		numBuckets = numBuckets*2;
		for(int i=0;i<temp.size();i++) {
			
			MapNode<K,V> head = temp.get(i);
			
			while(head != null) {
				
				K key = head.key;
				V value = head.value;
				insert(key,value);
				head = head.next;	
			}	
		}
	}
	
	public void insert(K key, V value) {
		
		int bucketIndex = getBucketIndex(key);
		MapNode<K,V> head = buckets.get(bucketIndex);
		
		while(head != null) {
			
			if(head.key.equals(key)) {
				
				head.value = value;
				return;
			}
	
			head = head.next;
		}
		
		head = buckets.get(bucketIndex);
		MapNode<K,V> newNode = new MapNode<>(key,value);
		newNode.next = head;
		buckets.set(bucketIndex,newNode);	
		count++;
		
		double loadfactor = (1.0*count)/numBuckets;
		
		if(loadfactor > 0.75) {
			
			reHash();
		}
	}
	
	public int size() {
		
		return count;
	}
	
	public V getValue(K key) {
		
		int bucketIndex = getBucketIndex(key);
		MapNode<K,V> head = buckets.get(bucketIndex);
		
		while(head != null) {
			
			if(head.key.equals(key)) {
				
				return head.value;
			}
	
			head = head.next;
		}
		
		return null;
	}

	
	public void removeKey(K key) {
		
		int bucketIndex = getBucketIndex(key);
		MapNode<K,V> head = buckets.get(bucketIndex);
		MapNode<K,V> prev = null;
	
		while(head != null) {
			
			if(head.key.equals(key)) {
				
				if (prev != null) prev.next = head.next;
				else buckets.set(bucketIndex,head.next);
				
				count--;
			}	
			prev = head;
			head = head.next;	
		}	
	}
        public void display(){
        for(int i = 0; i < size(); i++){
            if(buckets.get(i)!= null){
                MapNode<K, V> currentNode = buckets.get(i);
                while (currentNode != null){
                    System.out.println(String.format("Key is %s and value is %s", currentNode.key, currentNode.value));
                    currentNode = currentNode.next;
                }
            }
        }
    }
}
