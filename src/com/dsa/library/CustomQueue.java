/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dsa.library;



/**
 *
 * @author Mahboob Hasan
 */
import com.dsa.exceptions.EmptyQueueException;
import com.dsa.library.CustomStack;
public class CustomQueue<T> {
    int[] data;
    int front;
    int size;
    
    public CustomQueue(int cap) {
      data = new int[cap];
      front = 0;
      size = 0;
    }

    public int size() {
     
      return size;
    }

   

    // change this code
    public void enqueue(int val) {
      
      if(size == data.length){
        int[] ndata = new int[2 * data.length];
        for(int i = 0; i < size; i++){
          int idx = (front + i) % data.length;
          ndata[i] = data[idx];
        }
        data = ndata;
        front = 0;
      }

      int idx = (front + size) % data.length;
      data[idx] = val;
      size++;
    }

    public int dequeue() {
     
        try{
      if(size == 0){
        throw new EmptyQueueException("Queue underflow");
       
       } else {
        int val = data[front];

        front = (front + 1) % data.length;
        size--;

        return val;
       }
        }catch(EmptyQueueException e){
            e.printStackTrace();
        }
        return -1;
    }

    public int front() {
      
        try{
            if(size == 0){
        throw new EmptyQueueException("Queue underflow");
        
       } else {
        int val = data[front];
        return val;
       }
        }catch(EmptyQueueException e){
            e.printStackTrace();
        }
       return -1;
    }
    public boolean contains(CustomQueue<Integer> q,int value){
        while(q.size()>0){
            if(q.front==value) return true;
            q.dequeue();
        }
        return false;
    }
    public int center(CustomQueue<Integer> q){
        try{
            if(q.size==0)
                    throw new EmptyQueueException("Queue underflow");
            int n=q.size;
            int i=0;
            while(i!=n/2){
                q.dequeue();
                i++;
            }
            return q.dequeue();
            
        }catch(EmptyQueueException e){
            e.printStackTrace();
        }
         
        return -1;
    }
    
    
     // sorting queue
    
    public  void sort(CustomQueue<Integer> list)
    {
        for(int i = 1; i <= list.size(); i++)
        {
            int min_index = minIndex(list,list.size() - i);
            insertMinToRear(list, min_index);
        }
    }
    // for sorting queue
    public static int minIndex(CustomQueue<Integer> list,
                                     int sortIndex)
    {
    int min_index = -1;
    int min_value = Integer.MAX_VALUE;
    int s = list.size();
    for (int i = 0; i < s; i++)
    {
        int current = list.front();
         
        // This is dequeue() in Java STL
        list.dequeue();
 
        // we add the condition i <= sortIndex
        // because we don't want to traverse
        // on the sorted part of the queue,
        // which is the right part.
        if (current <= min_value && i <= sortIndex)
        {
            min_index = i;
            min_value = current;
        }
        list.enqueue(current);
    }
    return min_index;
}
    //for sorting queue
    public void insertMinToRear(CustomQueue<Integer> q,
                                             int min_index)
     {
        int min_value = 0;
        int s = q.size();
        for (int i = 0; i < s; i++)
        {
        int current = q.front();
        q.dequeue();
        if (i != min_index)
            q.enqueue(current);
        else
            min_value = current;
        }
        q.enqueue(min_value);
    }
    
    
    public void reverse(CustomQueue<Integer> q){
        CustomStack<Integer> st=new CustomStack<>();
        while(q.size()>0){
            st.push(q.dequeue());
           
        }
        while(!st.isEmpty()){
            q.enqueue(st.peek());
            st.pop();
                
        }
    }
    
     public void display(CustomQueue<Integer> q) {
      
     while(q.size()>0){
         System.out.print(q.dequeue()+" ");
     }
      
    }
  }